﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Security;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace SendBillUpload
{
    partial class BillUploadTrigger : ServiceBase
    {
        private System.Timers.Timer timer1;
        private string timeString;
        public int getCallType;
        string Url = "";
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public BillUploadTrigger()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            int strTime = Convert.ToInt32(ConfigurationManager.AppSettings["callDuration"]);
            getCallType = Convert.ToInt32(ConfigurationManager.AppSettings["CallType"]);
            if (getCallType == 1)
            {
                timer1 = new System.Timers.Timer();
                double inter = (double)GetNextInterval();
                log.Info("interval time in Milliseconds" + inter);
                timer1.Interval = inter;
                timer1.Elapsed += new ElapsedEventHandler(ServiceTimer_Tick);
                //SActivateService();
            }
            else
            {
                timer1 = new System.Timers.Timer();
                double inter = strTime * 1000;
                log.Info("interval time in Milliseconds" + inter);
                timer1.Interval = inter;
                timer1.Elapsed += new ElapsedEventHandler(ServiceTimer_Tick);
            }
            timer1.AutoReset = true;
            timer1.Enabled = true;
            //SendMailService.WriteErrorLog("Service started");
            log.Info("Service Started");
        }
        private void ServiceTimer_Tick(object sender, System.Timers.ElapsedEventArgs e)
        {

            try
            {
                ///send Post Bill Upload request
                ActivateService();
            }
            catch (Exception ex)
            {
                log.Error(ex);
            }
        }
        protected override void OnStop()
        {
            // TODO: Add code here to perform any tear-down necessary to stop your service.
        }
        public void OnDebug()
        {
            OnStart(null);
        }
        public void InitializeServiceConfiguration()
        {

            Url = ConfigurationManager.AppSettings["URL"];
        }

        public void ActivateService()
        {

            StringBuilder sbLog = new StringBuilder();
            try
            {
                timer1.Stop();
                log.Info("Activated the service...");

                //Get service configuration value from DB
                InitializeServiceConfiguration();
                PostBillUploadAPiRequest();

            }
            catch (Exception ex)
            {
                log.Error(ex);

            }
            finally
            {
                if (getCallType == 1)
                {

                    log.Info("Service stoped");
                    System.Threading.Thread.Sleep(1000000);
                    SetTimer();

                }
                else
                {
                    timer1.Start();
                    log.Info("Timer started");
                }
            }

        }


        private double GetNextInterval()
        {
            timeString = ConfigurationManager.AppSettings["StartTime"];
            DateTime t = DateTime.Parse(timeString);
            TimeSpan ts = new TimeSpan();
            int x;
            ts = t - System.DateTime.Now;

            if (ts.TotalMilliseconds < 0)
            {
                ts = t.AddDays(1) - System.DateTime.Now;//Here you can increase the timer interval based on your requirments.   
            }
            log.Info("Next Interval : " + ts);
            return ts.TotalMilliseconds;
        }

        /////////////////////////////////////////////////////////////////////
        private void SetTimer()
        {
            try
            {
                double inter = (double)GetNextInterval();
                timer1.Interval = inter;
                timer1.Start();
                log.Info("Timer started");
            }
            catch (Exception ex)
            {
                log.Error(ex);
            }
        }
        private void PostBillUploadAPiRequest()
        {

           // int flag = 0, count = 0;
            string data = "";

            try
            {
               // do
                //{
                    var link = string.Format(Url);
                  //  if (count > 0)
                    //{
                    //    log.Info("retry..." + count);
                    //}
                    //count++;

                    using (var client = new HttpClient())
                    {
                        ServicePointManager.ServerCertificateValidationCallback = new
                                                    RemoteCertificateValidationCallback
                                                    (
                                                       delegate { return true; }
                                                    );
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                        //client.SecurityProtocol = SecurityProtocolType.Tls12;
                        client.Timeout = TimeSpan.FromMinutes(20);
                        var uri = new Uri(link);
                        var Send = client.GetAsync(uri).ContinueWith(task =>
                        {
                            if (task.Status == TaskStatus.RanToCompletion)
                            {
                                var responce = task.Result;
                                if (responce.IsSuccessStatusCode)
                                {
                                   // flag = 1;
                                    data = responce.Content.ReadAsStringAsync().Result;
                                    log.Info("response" + data);
                                }
                            }

                        });
                        Send.Wait();
                    }
               // } while (count <= 5 && flag == 0);
                //var status = InsertIntoSMSTrack(data, userID, messageTemplate);
            }
            catch (Exception ex)
            {
                var a = ex;
                log.Error(ex);
            }
        }
    }
}
