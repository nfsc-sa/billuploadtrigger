﻿namespace WindowsServiceProject1
{
    partial class ProjectInstaller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Prop_SendBillUploadSvc = new System.ServiceProcess.ServiceProcessInstaller();
            this.SendBillUploadSvc = new System.ServiceProcess.ServiceInstaller();
            // 
            // Prop_SendBillUploadSvc
            // 
            this.Prop_SendBillUploadSvc.Account = System.ServiceProcess.ServiceAccount.LocalSystem;
            this.Prop_SendBillUploadSvc.Password = null;
            this.Prop_SendBillUploadSvc.Username = null;
            this.Prop_SendBillUploadSvc.AfterInstall += new System.Configuration.Install.InstallEventHandler(this.serviceProcessInstaller1_AfterInstall);
            // 
            // SendBillUploadSvc
            // 
            this.SendBillUploadSvc.ServiceName = "SendBillUpload";
            this.SendBillUploadSvc.StartType = System.ServiceProcess.ServiceStartMode.Automatic;
            this.SendBillUploadSvc.AfterInstall += new System.Configuration.Install.InstallEventHandler(this.SendBillUpload_AfterInstall);
            // 
            // ProjectInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.Prop_SendBillUploadSvc,
            this.SendBillUploadSvc});

        }

        #endregion

        private System.ServiceProcess.ServiceProcessInstaller Prop_SendBillUploadSvc;
        private System.ServiceProcess.ServiceInstaller SendBillUploadSvc;
    }
}